<?php

namespace App\Http\Controllers;

use App\Http\Requests\ArticlesRequest;

use App\Services\ArticleService;

use App\Traits\ApiResponser;

class ArticlesController extends Controller
{
    use ApiResponser;

    protected $articleService;

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct(ArticleService $articleService)
    {
        $this->articleService = $articleService;
    }

    public function index() {
        $articles = $this->articleService->getArticles();

        return $this->apiResponse('Articles successfully retrieved', $articles, 200);
    }

    public function show($id) {
        $article = $this->articleService->showArticle($id);

        if (!$article) {
            return $this->apiResponse('Article not found', null, 404);
        }

        return $this->apiResponse('Article successfully retrieved', $article, 200);
    }

    public function create(ArticlesRequest $request) {
        $validated = $request->validated();
        $article = $this->articleService->createArticle($validated);

        return $this->apiResponse('Article successfully created', $article, 201);
    }

    public function update(ArticlesRequest $request, $id) {
        $validated = $request->validated();
        $article = $this->articleService->updateArticle($id, $validated);
        
        if (!$article) {
            return $this->apiResponse('Article not found', null, 404);
        }
        
        return $this->apiResponse('Article successfully updated', $article, 200);
    }

    public function delete($id) {
        $this->articleService->deleteArticle($id);

        return $this->apiResponse('Article successfully deleted', null, 200);
    }
}
