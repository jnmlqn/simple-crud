<?php

namespace App\Services;

use App\Interfaces\ArticleRepositoryInterface;

class ArticleService
{

	protected $articleRepository;

	public function __construct(ArticleRepositoryInterface $articleRepository) {
		$this->articleRepository = $articleRepository;
	}

	public function getArticles() {
		return $this->articleRepository->index();
	}

	public function showArticle($id) {
		return $this->articleRepository->show($id);
	}

	public function createArticle($data) {
		return $this->articleRepository->create($data);
	}

	public function updateArticle($id, $data) {
		return $this->articleRepository->update($id, $data);
	}

	public function deleteArticle($id) {
		return $this->articleRepository->delete($id);
	}
}