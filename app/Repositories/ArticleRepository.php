<?php

namespace App\Repositories;

use App\Interfaces\ArticleRepositoryInterface;

use App\Models\Article;

class ArticleRepository implements ArticleRepositoryInterface
{

	protected $article;

	public function __construct(Article $article) {
		$this->article = $article;	
	}

	public function index() {
		$articles = $this->article->orderBy('created_at', 'DESC')->paginate(10);

		return $articles;
	}

    public function show($id) {
        $article = $this->article->find($id);

        return $article;
    }

	public function create($data) {
		$article = $this->article->create($data);

		return $article;
	}

	public function update($id, $data) {
		$article = $this->article->find($id);

		if ($article) {
            $article->fill($data)->save();
        }

		return $article;
	}

    public function delete($id) {
        $this->article->destroy($id);

        return;
    }
}