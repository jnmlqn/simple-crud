<?php

namespace App\Interfaces;


use Illuminate\Database\Eloquent\Model;


interface ArticleRepositoryInterface
{
   public function index();

   public function create(array $attributes);

   public function show($id);

   public function update($id, array $attributes);

   public function delete($id);
}
