<?php

use Laravel\Lumen\Testing\DatabaseMigrations;
use Laravel\Lumen\Testing\DatabaseTransactions;

use Illuminate\Support\Str;

class ArticlesApiTest extends TestCase
{
    /**
     * A basic test example.
     *
     * @return void
     */
    public function testCreateArticle() {
        $data = [
            'title' => 'Sample Article From Test 1',
            'body' => 'Sample Article From Test 1'
        ];

        $response = $this->post('/articles', $data);
        $this->assertEquals(201, $this->response->status());
    }

    public function testUpdateArticle() {
        $data = [
            'title' => 'Sample Article Update From Test',
            'body' => 'Sample Article Update From Test'
        ];

        $response = $this->post('/articles/2c9f9d74-d432-40b3-86b3-1d425f9dcd52', $data);
        $this->assertEquals(200, $this->response->status());
    }

    public function testCreateIfEmpty() {
        $response = $this->get('/articles');
        $this->assertNotEmpty($response);
    }
}
